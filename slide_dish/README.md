# Petri dish holder

This is an adapter shared by Pelle in the [openflexure forum](https://openflexure.discourse.group/t/stage-modifications/563) to switch easily between a microscope slide and a petri dish, on the OpenFlexure Microscope.

The forum post is copied below in case the link breaks:

> Hi everyone,
> 
> I love microscopes and 3d-printers as much as the next person.
> 
> I am using the OpenFlexure Microscope and I am switching a lot between slide and petri dish.
> 
> Today I put together this small piece (slide_dishV4.stl (28.2 KB)) making it easier for me to switch between slide and dish. It centers the slide but the dish frame had to be moved off center due to the screws. It is definitely not the best design but it fits the purpose for using 25.4mm x 76.2mm slide and 50mm diam petri dish.
> 
> This made me curious about other stage modifications made by community members. Unfortunately, I haven’t been able to find much here on the forum nor on OpenFlexure - GitLab.
> 
> So for anyone who has put together some microscope stage modification (for whatever application), I would be really happy to see it, it is always inspiring with other people’s designs. Who knows, it might be the piece/solution/inspiration one has been looking for.
> 
> Best regards
> 
> Per