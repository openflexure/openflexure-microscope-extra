# Dashcam and webcam mounts

The files in this folder were made by [Bongo Tech & Research Labs, Tanzania](https://www.btech.co.tz/) to allow the use of a dashcam, or low cost webcam, with the OpenFlexure Microscope.  They should work with v6.1 of the microscope body.

Credit to Grace Anyelwisye, Valerian Sanga, and Paul Nyakyi.